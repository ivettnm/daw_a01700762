function caso1(num)
{
	var table = document.createElement("table");
	var tbody = document.createElement("tbody");
	table.appendChild(tbody);
	var n = prompt("Ingresa un número");

	for(var i = 1; i <= n; i++)
	{
		let row = document.createElement("tr");
		let td = document.createElement("td");

		td.appendChild(document.createTextNode(i));
		row.appendChild(td);

		td = document.createElement("td");
		td.appendChild(document.createTextNode(i*i));
		row.appendChild(td);

		td = document.createElement("td");
		td.appendChild(document.createTextNode(i*i*i));
		row.appendChild(td);

		tbody.appendChild(row);
	}

	//document.body.appendChild(table);

	let div = document.getElementById("ejercicio1_res");
	div.appendChild(table);
}

function aleatorios()
{
	var x = Math.floor(Math.random() * 100 + 1);
	var y = Math.floor(Math.random() * 100 + 1);
	/*
	var n = 0;
	var l = document.getElementById("number");
	window.setInterval(function()
	{ l.innerHTML = n;
	  n++;
	},1000);
	*/
	var tiempoA = new Date();
	var seg = tiempoA.getSeconds();


	var k = prompt("La suma de " + x + "+" + y + " es igual a:");

	var tiempoB = new Date();
	var totalSeg = tiempoB.getSeconds();
	var final = totalSeg - seg;

	if(x+y == k)
		alert("¡CORRRRRRRRRECTOOOO! Tardaste " + final + " segundos.");


	else
		alert("TERRIBLE :(  Tardaste " + final + " segundos.");
}

function contador(a)
{
	var negativos = 0;
	var positivos = 0;
	var ceros = 0;

		for (var i = a.length -1; i>= 0; i--)
		{
			if(a[i] > 0)
			{
				positivos++;
			}
			else if (a[i] === 0)
			{
				ceros++;
			}
			else 
			{
				negativos++;
			}

		}

	alert("Hay " +positivos+ " números positivos, " +negativos+ " números negativos, y " 
			+ceros+ " ceros en: " +a+ ".");

}

function pruebaFuncionContador()
{

	var arr = new Array(11);

	for(var i = 0; i <= arr.length -1 ; i++)
	{
		arr[i] = Math.floor(Math.random() * 10 -1);
		console.log(arr[i]);
	}
	contador(arr);

}
	
function promedio(mat)
{
	var arr = new Array();
	var sum = 0;
	var i, j;

	for(i = 0; i < mat.length; i++)
	{
		sum = 0;
		for (j = 0; j < mat[i].length; j++)
		{
			sum = sum + mat[i][j];
			alert(" elemento de localidad ["+i+ "] [" +j+ "] es: " +mat[i][j]+ ".");	
		}
		
		arr[i] = Math.floor(sum/j);
		alert("Promedio del renglón " +(i+1)+ " de la matriz: " +arr[i]);
	}

}

function pruebaPromedio()
{
	var a = new Array(3);
	for(var i = 0; i<= a.length -1; i++)
	{
		a[i] = new Array ( (Math.floor(Math.random() * 10 +1)), (Math.floor(Math.random() * 10 +1)), (Math.floor(Math.random() * 10 +1)) );
	}

	promedio(a);

}



function inverso(x)
{
	var a=0;
	var b=0;
	var c=0;
	a = Math.floor(x/100);
	x = x-(a*100);
	b =	Math.floor(x/10);
	x = x-(b*10);
	c = x;
	if(a===0)
	{
		alert( "número en orden inverso " +c+  +b+ ".");
	}
	else
	{
		alert( "número en orden inverso " +c+  +b+  +a+ ".");
	}

}

function pruebaInverso()
{
	var x = Math.floor(Math.random()*999 + 1);
	console.log(x);
	inverso(x);
}

function ejercicioLibre()
{
	var n  = prompt("Ingresa número de días del año");
	var min;
	var max;
    

    if(n>=7)
    {
        max = ((n/7)*2);
        if(n%7 > 2)
        {
            max+= 2;
        }
        else
            max += n%7;


        min = ((n/7)*2) + ((n%7)/6);
    }

    if(n<7)
    {
        max = n == 1 ? 1 : 2;
        min = n/6;
    }

    max = Math.floor(max);
    min = Math.floor(min);

    if(min === max)
    {
    	alert("Los marcianos descansan " +min+ " días.");
    }
    else
    {
	    alert("En el peor de los casos, descansarían " +min+ " días. En el mejor de los casos, " +max+ " días.");
	}
}